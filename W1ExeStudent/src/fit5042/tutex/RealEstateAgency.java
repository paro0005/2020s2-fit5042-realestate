package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Prateek
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	
    	try {
        	Property prop1 = new Property(1,"24 Boston Ave, Malvern East VIC 3145, Australia",2,150,420000);
			this.propertyRepository.addProperty(prop1);
			Property prop2 = new Property(2,"11 Bettina St, Clayton VIC 3168, Australia",3,352,360000);
			this.propertyRepository.addProperty(prop2);
	    	Property prop3 = new Property(3,"3 Wattle Ave, Glen Huntly VIC 3163, Australia",5,800,650000);
			this.propertyRepository.addProperty(prop3);
	    	Property prop4 = new Property(4,"3 Hamilton St, Bentleigh VIC 3204, Australia",2,170,435000);
			this.propertyRepository.addProperty(prop4);
	    	Property prop5 = new Property(5,"82 Spring Rd,Hampston East VIC 3188, Australia",1,60,825000);
			this.propertyRepository.addProperty(prop5);

    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	
    	
    	try {
			List<Property> agencyProp = this.propertyRepository.getAllProperties();
			for (int i = 0; i < agencyProp.size(); i++) {
				
	    	      System.out.println(agencyProp.get(i));
	    	    
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
        
    	 
    	try {
    		System.out.print("Enter the ID of the property you want to search: ");
        	Scanner sc= new Scanner(System.in);    //System.in is a standard input stream  
        	int id = sc.nextInt();
        	Property dispProp = this.propertyRepository.searchPropertyById(id);
        	if( dispProp == null) {
            	System.out.println("No property found with the given ID");
        	}
        	else {
            	System.out.println(dispProp);
        	}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
		}
	}
}

// Prateek Arora  -  30837294
